import { Component, OnInit } from '@angular/core';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  title = 'pedidos';
  resultado = 0;

  ngOnInit(): void {
    this.calcular('suma', 10, 5);
  }


  saludoConsola() {
    console.log('HOLA ANGULAR...');
  }

  calcular(operacion, valor1, valor2) {
    switch (operacion) {
      case 'suma':
        this.resultado = valor1 + valor2;
        break;
      case 'resta':
        this.resultado = valor1 - valor2;
        break;

    }

  }

}

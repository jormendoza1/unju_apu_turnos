import { Component, OnInit } from '@angular/core';
import { HeaderComponent } from '../header/header.component';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  nombre: string;

  constructor() { }

  ngOnInit(): void {
  }

  cargar() {
    console.log(this.nombre);
  }
}

import { EmpresaListadoComponent } from './empresa/empresa-listado/empresa-listado.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { EmpresaFormComponent } from './empresa/empresa-form/empresa-form.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
  },
  {
    path: 'empresa_nueva',
    component: EmpresaFormComponent,
  },
  {
    path: 'empresa_listado',
    component: EmpresaListadoComponent,
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }

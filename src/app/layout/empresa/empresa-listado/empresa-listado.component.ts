import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empresa-listado',
  templateUrl: './empresa-listado.component.html',
  styleUrls: ['./empresa-listado.component.scss']
})
export class EmpresaListadoComponent implements OnInit {
  empresas = [];

  constructor() { }

  ngOnInit(): void {
    this.verDatos();
  }

  verDatos() {
    this.empresas = JSON.parse(localStorage.getItem('datos'));
    console.log(this.empresas);
  }

}

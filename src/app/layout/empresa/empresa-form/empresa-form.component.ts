import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-empresa-form',
  templateUrl: './empresa-form.component.html',
  styleUrls: ['./empresa-form.component.scss']
})
export class EmpresaFormComponent implements OnInit {
  nombre = '';
  direccion = '';
  telefono = '';

  // ----

  empresaForm: FormGroup;
  arrayEmpresas = [];

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.empresaForm = this.formBuilder.group({
      nombre: ['', [Validators.required] ],
      direccion: [ '', [Validators.required]],
      telefono: [ '', [Validators.required]]
    });

  }



  enviar() {
    console.log(this.nombre);
    console.log(this.direccion);
    console.log(this.telefono);
  }








  enviarReactivo() {
    console.log(this.empresaForm.value);
    this.guardar();
  }









  guardar() {
    this.arrayEmpresas.push(this.empresaForm.value);
    localStorage.removeItem('datos');
    localStorage.setItem('datos', JSON.stringify(this.arrayEmpresas));
  }

  verDatos() {
    var guardado = localStorage.getItem('datos');
    console.log('objetoObtenido: ', JSON.parse(guardado));
  }


}
